var assert = require('assert');
var App = require('../public/App');
var TestClass = require('../public/TestClass');

// Test tests
describe('test', function() {
  it('return -1', function() {
    var v = -1;
    assert.equal(v, -1);
  });
});

// TestClass tests
describe('TestClass', function() {
  it('return undefined if constructor empty', function() {
    var testclass = new TestClass();
    assert.equal(testclass.text, undefined);
  });

  it('return text as passed in constructor', function() {
    var expected = 'test text';
    assert.equal(
      new TestClass(expected).value,
      expected
    );
  });
}); //TestClass

/* Test classes */

describe('Field class', function() {
  it('return ..', function() {
    var app = new App();
  });
});

describe('App class', function() {
  it('return ..', function() {
    var app = new App();
  });
});

describe('Box class', function() {
  it('return ..', function() {
    var app = new App();
  });
});

describe('App class', function() {
  it('return ..', function() {
    var app = new App();
  });
});

