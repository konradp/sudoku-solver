class Field {
  constructor(row, col) {
    this.row = row;
    this.col = col;
    this.obj = document.getElementById(
      String(row) + String(col)
    );
    if(this.obj == null)
      throw 'Field'+this.row+this.col+'does not exist.';
  }

  // Getters
  get value() {
    return this.obj.value;
  }
  set value(v) {
    if(v != null) this.obj.value = v;
    else throw 'Value not set.';
  }
} //Field

if(typeof module !== 'undefined'
  && typeof module.exports !== 'undefined'
) module.exports = Field;
else window.Field = Field;
