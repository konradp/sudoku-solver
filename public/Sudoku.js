'use strict';

var Field = require('./Field');
var Box = require('./Box');

class Sudoku {

constructor(grid) {
  console.log(typeof(grid));
  console.log(Array.isArray(grid));
  if(Array.isArray(grid))
    this.bArr = true;
  else
    this.bArr = false;


  // Init
  // boxwidth: compute it from window object
  this.fields = []; // [{ row, col, isPossible },.. ]
  var boxwidth = 3; // DEBUG
  this.boxwidth = boxwidth;
  this.size = boxwidth*boxwidth;
  this.boxes = this._initBoxes();
  this.fields = this._initFields();

  // Validate to ensure integers only
  console.log('Validate');
  this._validateFields(this.fields);

  // TESTING
  var l = [
    [1,1],[1,2],[1,3],
    [2,1],[2,2],[2,3]
  ];
  var m = [
    [2,4],[2,5],[2,6],
    [1,5],[1,6]
  ];
  var k = 1;
//  l.forEach((i) => {
//    var j = new Field(i[0], i[1]);
//    j.value = k;
//    k++;
//  })
//  var n = 1;
//  m.forEach((i) => {
//    var j = new Field(i[0], i[1]);
//    j.value = n;
//    n++;
//  })
}

solve() {
  console.log('Solve');
  var chg = true;
  while(chg) {
    chg = false; // Iterate until nothing new found
    // Taken items
    for(var i of this.fields) {
      // Fields
      console.log(JSON.stringify([i.row, i.col]));
      console.log(JSON.stringify(i.isPossible));
      var f = new Field(i.row, i.col);
      if(f.value != '') {
        // Field taken: Clear field, row, col, box
        if(this._clrField(i.row, i.col)) chg = true;
        if(this._clrRowCol('row', i.row, f.value)) chg = true;
        if(this._clrRowCol('col', i.col, f.value)) chg = true;
        if(this._clrInBox(i.row, i.col, f.value)) chg = true;
        console.log(JSON.stringify(i.isPossible)); //DEBUG
      } else {
        continue; // Skip empty field
      }
    } //for fields
    // Boxes
    this.boxes.forEach((box) => {
      if(this._clrBoxLines(box)) chg = true;
      if(this._clrBoxRemaining(box)) chg = true;
    })

    // Empty: Iterate over empty fields to see if can fill
    console.log('Iterate over empty fields');
    for(var i of this.fields) {
      var f = new Field(i.row, i.col);
      if(f.value != '') continue;
      else {
        // Empty field
        var remaining = [];
        for(var key in i.isPossible) {
          if(i.isPossible[key]) remaining.push(key);
        }
        // Only one value possible
        if(remaining.length == 1) {
          console.log(JSON.stringify('AAAA', remaining[0],i));
          f.value = remaining[0];
          // TODO: Check no duplicate fields in a box
          chg = true;
        }
      } //else
    } //for
  //break; // DEBUG. TODO: Remove this
  } //while
}

_clrField(row, col) {
  console.log('Clear field', row, col);
  var chg = false;
  var f = this.fields.find(obj => {
    return obj.row == row && obj.col == col;
  })
  for(var prop in f.isPossible) {
    if(f.isPossible[prop]) {
      f.isPossible[prop] = false;
      chg = true;
    }
  }
  return chg;
}

_clrRowCol(type, coord, value) {
  // Clear a row or a column for a given value
  // type: row/col, coord: 1-9, value: 1-9
  // Return true if changed any values
  var chg = false;
  this.fields.filter(obj => {
    return obj[type] === coord;
  }).forEach((i) => {
    if(i.isPossible[value]) {
      i.isPossible[value] = false;
      chg = true;
    }
  })
  if(chg) console.log('Clear',type,coord,'value',value);
  return chg;
}

_clrRowColExcept(type, coord, value, exceptions) {
  console.log('Clear',type,coord,
    'value',value,'except',exceptions);
  // Clear a row or a column for a given value
  // except for the specified coordinates
  // type: row/col, coord: 1-9, value: 1-9,
  // exceptions: [1, 2, 3]
  // Return true if changed any values
  var chg = false;
  var btype = (type == 'row')? 'col' : 'row';
  this.fields.filter(obj => {
    return obj[type] === coord;
  }).forEach((i) => {
    console.log('holo',i[btype]);
    if(!exceptions.includes(i[btype]) && i.isPossible[value]) {
      // ISSUE HERE
      i.isPossible[value] = false;
      chg = true;
    }
  })
  if(chg)
    console.log('Clear',type,coord,
      'value',value,'except',exceptions);
  return chg;
}

_clrInBox(row, col, value) {
  // Mark a value as not possible for all fields of a box
  // Return true if changed a field
  var chg = false;
  var box = new Box(row, col),
      f,
      tmp;
  box.fields.forEach((f) => {
    tmp = this.fields.find(obj => {
      return obj.row == f.row && obj.col == f.col;
    })
    if(tmp.isPossible[value]) {
      tmp.isPossible[value] = false;
      chg = true;
    }
  })
  if(chg) console.log('Clear box',row,col,'value',value);
  return chg;
}

_clrBoxLines(box) {
  // Find and clear blocking rows/cols
  var chg = false;

  // Array of fields which can accomodate given number
  var isPossible = {};
  var f;
  for(var i=1; i<=this.size; i++) {
    // For each number, get fields belonging to the box
    box.fields.forEach((field) => {
      f = this.fields.find(obj => {
        return obj.row == field.row && obj.col == field.col;
      })
      // Get only the possible
      if(f.isPossible[i]) {
        if(isPossible[i] === undefined)
          isPossible[i] = [];
        isPossible[i].push({
          row: f.row,
          col: f.col
        })
      }
    })
  } //for number

  var types = ['row', 'col'];
  types.forEach((type) => {
    var coords = [];
    for(var number in isPossible) {
      // Get 1D coordinates
      box.fields.forEach((f) => {
        if(!coords.includes(f[type]))
          coords.push(f[type]);
      })

      var i = 0,
          same = true,
          last = null,
          tmp;
      for(var field in isPossible[number]) {
        tmp = isPossible[number][field]
        if(last == null) last = tmp;
        if(tmp[type] != last[type]) same = false;
        else i++;
      }
      if(same && i > 1) {
        if(this._clrRowColExcept(type, tmp[type], number, coords))
          chg = true;
      }
    }
  })
  return chg;
}

_clrBoxRemaining(box) {
  // Walk through all possible values in a box. If, based on box's constraints
  // there remains only one field which can accomodate it, block other fields
  // Example: If a field has possible value of 1 or 2, but 2 cannot be assigned
  // to any other field in the box, choose value 1
  var chg = false;
  for(var val=1; val<=this.size; val++) { // 1-9
    var count = 0,
        row,
        col;
    box.fields.forEach((j) => {
      if(this._getField(j.row, j.col).isPossible[val]) {
        // Found field that is fillable
        count++;
        row = j.row;
        col = j.col;
      }
    })
    if(count == 1) {
      // Found an unique fillable field; clear others
      chg = true;
      var f;
      console.log('FoundInBox',val,'at',row,col);
      box.fields.forEach((j) => {
        f = this._getField(j.row, j.col);
        if(f.row == row && f.col == col) {
          // Unique field, clear all other values
          for(var prop in f.isPossible) {
            if(prop != val) f.isPossible[prop] = false;
          }
        } else {
          // Other fields, value already taken, so clear
          f.isPossible[val] = false;
        }
      })
    } //if
  } //for 1-9
  return chg;
}

_getField(row, col) {
  return this.fields.find(obj => {
    return obj.row == row && obj.col == col;
  })
}

_initBoxes() {
  // Create nine boxes by initialising them from any
  // field belonging to that box
  var arr = [];
  for(var i=1; i<=this.boxwidth; i++)
    for(var j=1; j<=this.boxwidth; j++) {
      arr.push(
        new Box(i*this.boxwidth, j*this.boxwidth),
      );
    }
  return arr;
}

_initFields() {
  // Return array of coords and isPossible
  var arr = [], possible = {};
  for(var i=1; i<=this.size; i++) possible[i] = true;
  for(var i=1; i<=this.size; i++)
    for(var j=1; j<=this.size; j++) {
      arr.push({
        row: i,
        col: j,
        isPossible: JSON.parse(JSON.stringify(possible))
      });
    }
  return arr;
}

_validateFields() {
  var v;
  for(var i of this.fields) {
    var f = new Field(i.row, i.col);
    if(f.value != '') {
      if(!isNaN(f.value)) {
        v = parseInt(f.value);
        if(v>=1 && v<=this.size) continue;
      }
    } else {
      continue;
    }
    // If we got here, there were invaild entries
    alert('Found invalid entries. Aborted.');
    break;
  };
  return;
}

} //Sudoku

if(typeof module !== 'undefined'
  && typeof module.exports !== 'undefined'
) module.exports = Sudoku;
else window.Sudoku = Sudoku;
