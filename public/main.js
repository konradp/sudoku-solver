'use strict';
// Width of a box. When squared, gives the maximum
// available number. E.g. in the usual sudoku, it's
// 3 * 3 = 9
global.WIDTH = 3; // global

// INCLUDE
var Sudoku = require('./Sudoku');

// Main
var run = function run(e) {
  e.preventDefault();
var WIDTH = 3; // global
  var sudoku = new Sudoku(WIDTH);
  sudoku.solve();
}

window.onload = function() {
  // Attach listener to form
  var form = document.getElementById('sudoku-form');
  form.addEventListener('submit', run);
};
