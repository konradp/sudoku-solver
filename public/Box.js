'use strict';

class Box {

constructor(row, col) {
  this.w = WIDTH;
  this.size = this.w * this.w;
  this.dr = Math.floor((row-1)/3)+1;
  this.dc = Math.floor((col-1)/3)+1;
}

// Getters
get fields() { return this._getFields(); } // [ { row: 1, col: 2 }, ... ]
get remaining() { return this._getRemaining(); }

// PRIVATE
_getRemaining() {
  // Return free values in a box, e.g. empty box
  // returns 1,...,9, full box returns nothing
  // Output: { 1: true, 2: false, ... }
  var ret = {},
      w = this.w;
  for(var i=1; i<=(w*w); i++)
    ret[i] = true;
    this.fields.forEach((i) => {
      var f = new Field(i.row, i.col);
      if(f.value != '') ret[f.value] = false
  });
  return ret;
}

_getFields() {
  // Return all nine fields of the box
  var dr = this.dr,
      dc = this.dc,
      w = this.w,
      fields = [];
  for(var i=1; i<=w; i++)
    for(var j=1; j<=w; j++) {
      fields.push({
        row: w*(dr-1)+i,
        col: w*(dc-1)+j
      })
    }
  return fields;
}

} //Box

if(typeof module !== 'undefined'
  && typeof module.exports !== 'undefined'
) module.exports = Box;
else window.Box = Box;
