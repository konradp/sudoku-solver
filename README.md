# Sudoku solver (WORK IN PROGRESS)

A Sudoku solver written in javascript.  
https://konradp.gitlab.io/sudoku-solver

# Development
Install dependencies and run on local machine with `npm`.
```
npm start
```
Run the app in the browser.  
https://localhost:8080
